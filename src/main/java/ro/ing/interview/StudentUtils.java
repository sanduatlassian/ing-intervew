package ro.ing.interview;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@SuppressWarnings("ALL")
public class StudentUtils {

    // design the class as a singleton
    private StudentUtils() {}

    private static class SingletonHelper {
        private static final StudentUtils INSTANCE = new StudentUtils();
    }

    public static StudentUtils getInstance() {
        return SingletonHelper.INSTANCE;
    }

    // fields
    private final String firstYearStudentsFile = "firstYearStudentsGrades.json";
    private final String secondYearStudentsFile = "secondYearStudentsGrades.json";

    private String fileContent;

    private final String praiseEmailTemplate = "./average-ok.eml.vt";
    private final String needImprovementGradeEmailTemplate = "./average-ni.eml.vt";

    private EmailSender emailSender;

    @Getter
    @Setter
    public static class StudentList {
        private List<StudentUtils.StudentRecord> studentRecords;
    }

    @Getter
    @Setter
    public static class StudentRecord {
        private int id;
        private String name;
        private String surname;
        private Date dateOfBirth;
        private boolean active;
        private float grade;
        private String subject;
        private String emailAddress;
    }

    @Getter
    @Setter
    public static class EmailSender {
        private String sender;

        public boolean sendEmail(String emailAddress, String subject, String textMessage) {
            return true;
        }
    }

    /*
    Retrieves an ordered list of active students as JSON
     */
    public String getFirstYearActiveStudentsAsJSON() {

        File f = new File(firstYearStudentsFile);

        try {
            List<String> strings = Files.readAllLines(Paths.get(f.toURI()));
            StringBuilder sb = new StringBuilder();
            for (String s1 : strings) {
                sb.append(s1);
                sb.append("\n");
            }
            fileContent = sb.toString();
            Gson gson = new Gson();
            StudentList studentList = gson.fromJson(fileContent, StudentList.class);

            List<StudentRecord> l = studentList.getStudentRecords()
                    .stream()
                    .filter(StudentRecord::isActive)
                    .collect(Collectors.toList());

            return gson.toJson(l);
        } catch (IOException ioe) {
            log.error("I/O error occurs reading from the file or a malformed or unmappable byte sequence is read");
            return null;
        } catch (SecurityException se) {
            log.error("read access to the file");
            return null;
        } catch (JsonSyntaxException jse) {
            log.error("json is not a valid representation for an object of type classOfT");
            return null;
        } catch (Exception e) {
            log.error("exception occurred");
            return null;
        }
    }

    /*
    Retrieves an ordered list of active students as JSON
     */
    public String getSecondYearActiveStudentsAsJSON() {

        try {
            List<String> myStrings = Files.readAllLines(Paths.get(new File(secondYearStudentsFile).toURI()));
            StringBuilder sb = new StringBuilder();
            for (String s1 : myStrings) {
                sb.append(s1);
                sb.append("\n");
            }
            fileContent = sb.toString();
            Gson gson = new Gson();
            StudentList studentList = gson.fromJson(fileContent, StudentList.class);

            List<StudentRecord> studentRecords = studentList.getStudentRecords()
                    .stream()
                    .filter(StudentRecord::isActive)
                    .collect(Collectors.toList());

            return gson.toJson(studentRecords);
        } catch (FileSystemNotFoundException e) {
            log.error("Runtime exception: a file system cannot be found");
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    /*
    Retrieves average grade for student and sends email with grade to student
     */
    public float calculateAverageGrade(String name, String surname, String studentEmail) throws RuntimeException, IOException {

        //readAllLines() throws
        //        - java.io.IOException -> Exception
        //        - SecurityException -> RuntimeException -> Exception

        //get() throws
        //        - IllegalArgumentException -> RuntimeException -> Exception
        //        - FileSystemNotFoundException -> RuntimeException
        //        - SecurityException -> RuntimeException
        List<String> myStrings = Files.readAllLines(Paths.get(new File(firstYearStudentsFile).toURI()));

        StringBuilder sb = new StringBuilder();
        for (String s1 : myStrings) {
            sb.append(s1);
            sb.append("\n");
        }
        fileContent = sb.toString();
        Gson gson = new Gson();
        StudentList studentList = gson.fromJson(fileContent, StudentList.class);

        int sum = 0;
        int gradeCount = 0;

        StudentRecord targetStudent = null;

        for (StudentRecord studentRecord : studentList.getStudentRecords()) {
            if (studentRecord.name == name && studentRecord.surname == surname) {
                sum += studentRecord.grade;
                gradeCount++;
                targetStudent = studentRecord;
            }
        }

        float avg = sum / gradeCount;

        try {
            VelocityEngine velocityEngine = new VelocityEngine();
            velocityEngine.init();

            VelocityContext context = new VelocityContext();
            context.put("firstName", name);
            context.put("lastName", surname);
            context.put("senderName", "John Doe");
            context.put("positionAndTitle", "Principal");

            // getTemplate() throws
            //        - ResourceNotFoundException -> VelocityException -> RuntimeException
            //        - ParseErrorException -> VelocityException -> RuntimeException
            Template template = null;
            if (avg > 3) template = velocityEngine.getTemplate(this.praiseEmailTemplate);
            else template = velocityEngine.getTemplate(this.needImprovementGradeEmailTemplate);

            StringWriter emailWriter = new StringWriter();
            template.merge(context, emailWriter);

            this.emailSender.sendEmail(targetStudent.emailAddress, "Grades", emailWriter.getBuffer().toString());
        } catch(Exception ex){
            throw new VelocityException("Exception occured loading template: ", ex);
        }

        return avg;

    }

}
